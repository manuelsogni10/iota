from iota import Iota
from iota import ProposedTransaction
from iota import Address
from iota import Tag
from iota import TryteString


api = Iota('https://nodes.mainnet.iota.org:443', testnet = True) 

address = 'CGFBKWQRQKYVXWWGBNZQMQ9FISYVR9BMJIQNGHOZHINURGACPITHJQBWPIEOQDNNB9NKLGFMNLM9JELJ9QZGDKZEHB'


message = TryteString.from_unicode('Hello Pol')


tx = ProposedTransaction(
address = Address(address),
message = message,
value = 0
)

result = api.send_transfer(transfers = [tx])

print(result['bundle'].tail_transaction.hash)
