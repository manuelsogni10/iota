from django.urls import path
from .views import homePageView
from .views import postPageView

urlpatterns = [
    path('', homePageView, name='home'),
    path('post', postPageView, name='post')
]
