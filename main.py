from iota import Iota
from iota import ProposedTransaction
from iota import Address
from iota import Tag
from iota import TryteString


api = Iota('https://nodes.iota.org:443') 

address = 'AHCBKPFTWJKMDVXWH9IKFKUGNXKEPRYKJBJKIGZGPDTIJDQVZLNXIYGECGZACBMOSCKKDVKBTRENVQLJAOLDFLBLXC'


message = TryteString.from_unicode(' PORCODDIO  ')


tx = ProposedTransaction(
address = Address(address),
message = message,
value = 0
)

result = api.send_transfer(transfers = [tx])

print(result['bundle'].tail_transaction.hash)
